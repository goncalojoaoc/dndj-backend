package discord

import authentication.IUserAuthRepository
import authentication.qr.IQRCodeGenerator
import discord.audio.AudioHandlers
import discord.audio.IAudioHandlers
import discord.message.IMessageMapper
import net.dv8tion.jda.api.entities.User
import net.dv8tion.jda.api.events.message.MessageReceivedEvent
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.mockito.kotlin.*

internal class MessageListenerTest {

    lateinit var sut: MessageListener

    @Mock
    lateinit var mockRepository: IUserAuthRepository

    @Mock
    lateinit var mockQRCodeGenerator: IQRCodeGenerator

    @Mock
    lateinit var mockMessageMapper: IMessageMapper

    @Mock
    lateinit var mockAudioHandlers: IAudioHandlers

    @Before
    fun setup() {
        MockitoAnnotations.openMocks(this)
        sut = MessageListener(
            repository = mockRepository,
            qrCodeGenerator = mockQRCodeGenerator,
            messageMapper = mockMessageMapper,
            audioHandlers = mockAudioHandlers
        )
    }

    @Test
    fun `commands from bot users should be ignored`() {
        val mockEvent = mock<MessageReceivedEvent>()
        val mockAuthor = mock<User>()
        whenever(mockEvent.author).thenReturn(mockAuthor)
        whenever(mockAuthor.isBot).thenReturn(true)

        sut.onMessageReceived(mockEvent)

        verify(mockAuthor).isBot
        verifyNoMoreInteractions(mockAuthor)
        verifyZeroInteractions(mockRepository, mockQRCodeGenerator)
    }
}