package base.models

import kotlinx.serialization.Serializable

@Serializable
data class ErrorResponse(
    val message: String,
    val serverMessage: String? = null
) {
    companion object {

        fun withMessage(message: String) = ErrorResponse(message)

        fun withException(userFacingMessage: String, error: Throwable?) = error?.let {
            ErrorResponse(
                message = userFacingMessage,
                serverMessage = "${error::class.simpleName}: ${error.message}"
            )
        } ?: withMessage(userFacingMessage)

        fun withException(error: Throwable) = ErrorResponse(
            message = error.message.toString(),
            serverMessage = "${error::class.simpleName}: ${error.message}"
        )
    }
}
