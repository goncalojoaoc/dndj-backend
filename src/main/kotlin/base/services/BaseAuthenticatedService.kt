package base.services

import authentication.IUserAuthRepository
import authentication.UnknownUserException
import authentication.models.AuthUser

abstract class BaseAuthenticatedService(
    private val authRepository: IUserAuthRepository
) {

    fun authenticateUser(key: String): AuthUser =
        authRepository.getAuthenticatedUser(key) ?: throw UnknownUserException("Invalid key")
}
