package extensions

import authentication.UnknownUserException
import base.models.ErrorResponse
import io.ktor.application.*
import io.ktor.http.*
import io.ktor.response.*
import io.ktor.util.pipeline.*

/**
 * Verifies an apiKey header was received, and tries to perform the requested action. Any exception will be caught and
 * sent back to the user
 */
suspend inline fun PipelineContext<Unit, ApplicationCall>.doSafely(action: (String) -> Unit) {
    val key = call.request.headers["apiKey"]
    if (key.isNullOrEmpty()) {
        call.respond(HttpStatusCode.Unauthorized, ErrorResponse.withMessage("Missing key"))
    } else {
        try {
            action(key)
        } catch (ex: UnknownUserException) {
            call.respond(HttpStatusCode.Forbidden, ErrorResponse.withException(ex))
        } catch (ex: IllegalArgumentException) {
            call.respond(HttpStatusCode.BadRequest, ErrorResponse.withException(ex))
        } catch (ex: Exception) {
            call.respond(HttpStatusCode.InternalServerError, ErrorResponse.withException(ex))
        }
    }
}

fun ApplicationCall.param(id: String): String =
    parameters[id] ?: throw IllegalArgumentException("Missing $id")