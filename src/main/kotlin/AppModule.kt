import authentication.IUserAuthRepository
import authentication.JsonUserAuthRepository
import authentication.qr.IQRCodeGenerator
import authentication.qr.QRCodeGenerator
import channels.VoiceChannelOpsService
import channels.VoiceChannelsService
import channels.models.VoiceChannelModelMapper
import com.sedmelluq.discord.lavaplayer.player.AudioPlayerManager
import com.sedmelluq.discord.lavaplayer.player.DefaultAudioPlayerManager
import discord.Discord
import discord.MessageListener
import discord.audio.AudioHandlers
import discord.audio.GuildAudioPlayerManager
import discord.audio.IAudioHandlers
import discord.message.IMessageMapper
import discord.message.MessageMapper
import org.koin.dsl.module
import tracks.TracksService
import tracks.models.mapper.QueueModelMapper
import tracks.models.mapper.TrackModelMapper

val appModule = module {
    single<IUserAuthRepository> { JsonUserAuthRepository(true) }
    factory<IQRCodeGenerator> { QRCodeGenerator() }

    single<AudioPlayerManager> { DefaultAudioPlayerManager() }
    single { GuildAudioPlayerManager(get()) }
    factory<IAudioHandlers> { AudioHandlers(get()) }

    single { MessageListener(get(), get(), get(), get()) }
    single { Discord(get()) }
    factory<IMessageMapper> { MessageMapper() }

    factory { VoiceChannelsService(get(), get(), get(), get()) }
    factory { VoiceChannelOpsService(get(), get(), get(), get()) }
    factory { TracksService(get(), get(), get(), get(), get()) }

    factory { TrackModelMapper() }
    factory { VoiceChannelModelMapper() }
    factory { QueueModelMapper(get()) }
}