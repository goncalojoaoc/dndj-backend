package channels

import base.models.ErrorResponse
import extensions.doSafely
import extensions.param
import io.ktor.application.*
import io.ktor.http.*
import io.ktor.response.*
import io.ktor.routing.*
import org.koin.ktor.ext.inject

fun Route.registerChannelsRoutes() {
    voiceChannelsRoute()
    channelOpsRoutes()
}

fun Route.voiceChannelsRoute() {
    val service: VoiceChannelsService by inject()

    route("/voiceChannels") {
        get {
            doSafely { apiKey ->
                call.respond(service.getVoiceChannelsForUser(apiKey))
            }
        }

        route("/current") {
            get {
                doSafely { apiKey ->
                    call.respond(service.currentChannel(apiKey))
                }
            }
        }
    }
}

fun Route.channelOpsRoutes() {

    val service: VoiceChannelOpsService by inject()

    route("/voiceChannels") {
        route("/join/{channelId}") {
            post {
                doSafely { apiKey ->
                    val channelId = call.param("channelId")
                    val joined = service.joinChannel(key = apiKey, channelId = channelId)
                    call.respond(joined)
                }
            }
        }
        route("/leave/{channelId}") {
            post {
                doSafely { apiKey ->
                    val channelId = call.param("channelId")
                    val left = service.leaveChannel(apiKey, channelId)
                    call.respond(left)
                }
            }
        }
    }

}
