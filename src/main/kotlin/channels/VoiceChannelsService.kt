package channels

import authentication.IUserAuthRepository
import base.services.BaseAuthenticatedService
import channels.models.CurrentVoiceChannelResponse
import channels.models.VoiceChannelModel
import channels.models.VoiceChannelModelMapper
import discord.Discord
import discord.audio.IAudioHandlers

class VoiceChannelsService(
    private val discord: Discord,
    private val audioHandlers: IAudioHandlers,
    authRepository: IUserAuthRepository,
    private val voiceChannelModelMapper: VoiceChannelModelMapper
) : BaseAuthenticatedService(authRepository) {

    fun getVoiceChannelsForUser(key: String): List<VoiceChannelModel> {
        val user = authenticateUser(key)
        val channels = discord.guildFromId(user.guildId)?.voiceChannelCache?.toList() ?: emptyList()
        return voiceChannelModelMapper.map(channels)
    }

    fun currentChannel(key: String): CurrentVoiceChannelResponse {
        val guildId = authenticateUser(key).guildId
        val guild = discord.guildFromId(guildId)

        val channel = guild?.let {
            audioHandlers.get(guild)?.let {
                if (it.isConnected) {
                    it.connectedChannel?.let { voiceChannelModelMapper.map(it) }
                } else {
                    null
                }
            }
        }

        return CurrentVoiceChannelResponse(
            currentVoiceChannel = channel,
            currentGuild = guild?.let { voiceChannelModelMapper.mapGuild(it) }
        )
    }
}