package channels

import authentication.IUserAuthRepository
import base.services.BaseAuthenticatedService
import channels.models.VoiceChannelModel
import channels.models.VoiceChannelModelMapper
import discord.Discord
import discord.audio.AudioHandlers
import discord.audio.IAudioHandlers
import org.slf4j.LoggerFactory

class VoiceChannelOpsService(
    private val discord: Discord,
    private val audioHandlers: IAudioHandlers,
    authRepository: IUserAuthRepository,
    private val voiceChannelModelMapper: VoiceChannelModelMapper
) : BaseAuthenticatedService(authRepository) {

    private val log = LoggerFactory.getLogger(this::class.java)

    fun joinChannel(key: String, channelId: String): VoiceChannelModel {
        val guildId = authenticateUser(key).guildId

        val voiceChannel = discord.channelFromId(channelId)
        val guild = discord.guildFromId(guildId)

        if (voiceChannel == null || guild == null) {
            log.error("Error joining channel. ID: $channelId - $voiceChannel | Guild ID: $guildId - $guild")
            throw IllegalStateException("Could not fetch guild or channel info")
        }

        if (voiceChannel.guild.id != guildId) {
            val msg = "Channel with ID $channelId does not belong to authorized guild - ${guild.name} (ID: $guildId)"
            log.warn(msg)
            throw IllegalArgumentException(msg)
        }

        audioHandlers.get(guild)?.connect(voiceChannel)
            ?: throw IllegalStateException("Error fetching an audio handler")

        return voiceChannelModelMapper.map(voiceChannel)
    }

    fun leaveChannel(key: String, channelId: String): VoiceChannelModel {
        val guildId = authenticateUser(key).guildId

        val voiceChannel = discord.channelFromId(channelId)
        val guild = discord.guildFromId(guildId)

        if (voiceChannel == null || guild == null) {
            log.error("Error leaving channel. ID: $channelId - $voiceChannel | Guild ID: $guildId - $guild")
            throw IllegalStateException("Something went wrong")
        }

        audioHandlers.get(guild)?.disconnect()
            ?: throw IllegalStateException("Error fetching an audio handler")

        return voiceChannelModelMapper.map(voiceChannel)
    }
}
