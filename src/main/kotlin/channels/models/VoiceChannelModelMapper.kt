package channels.models

import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.entities.channel.concrete.VoiceChannel

class VoiceChannelModelMapper {

    fun map(domain: List<VoiceChannel>) = domain.map { map(it) }

    fun map(domain: VoiceChannel): VoiceChannelModel = VoiceChannelModel(
        id = domain.id,
        name = domain.name
    )

    fun mapGuild(domain: Guild) = GuildModel(
        id = domain.id,
        name = domain.name
    )
}
