package channels.models

import kotlinx.serialization.Serializable

@Serializable
data class CurrentVoiceChannelResponse(
    val currentVoiceChannel: VoiceChannelModel?,
    val currentGuild: GuildModel?
)
