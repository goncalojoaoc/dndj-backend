package channels.models

import kotlinx.serialization.Serializable

@Serializable
data class GuildModel(
    val id: String,
    val name: String
)
