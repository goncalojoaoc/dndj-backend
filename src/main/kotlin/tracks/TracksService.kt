package tracks

import authentication.IUserAuthRepository
import base.services.BaseAuthenticatedService
import discord.Discord
import discord.audio.AudioHandlers
import discord.audio.GuildAudioHandler.SearchResultCallback
import discord.audio.IAudioHandlers
import tracks.models.*
import tracks.models.mapper.QueueModelMapper
import tracks.models.mapper.TrackModelMapper
import org.slf4j.LoggerFactory
import kotlin.coroutines.Continuation
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine

class TracksService(
    private val audioHandlers: IAudioHandlers,
    authRepository: IUserAuthRepository,
    private val queueModelMapper: QueueModelMapper,
    private val trackModelMapper: TrackModelMapper,
    private val discord: Discord
) : BaseAuthenticatedService(authRepository) {

    private val notConnectedException = IllegalStateException("Must be connected to a voice channel first")

    private val log = LoggerFactory.getLogger(this::class.java)

    suspend fun queue(
        apiKey: String,
        request: TrackRequest
    ) = suspendCoroutine<TrackQueuedResponse> { continuation ->
        val user = authenticateUser(apiKey)
        val guild = discord.guildFromId(user.guildId)

        guild?.let {
            audioHandlers.get(guild)?.queue(
                query = request.query,
                clearPrevious = request.startImmediately,
                callback = searchResultCallback(request.query, continuation)
            ) ?: continuation.resumeWithException(notConnectedException)
        }
    }

    fun clearQueue(
        apiKey: String
    ) {
        val user = authenticateUser(apiKey)
        val guild = discord.guildFromId(user.guildId)

        guild?.let { audioHandlers.get(guild)?.clear() ?: throw notConnectedException }
    }

    fun getQueue(
        apiKey: String
    ): TrackQueueModel {
        val user = authenticateUser(apiKey)
        val guild = discord.guildFromId(user.guildId)

        val audioHandler = guild?.let { audioHandlers.get(guild) } ?: throw notConnectedException
        return queueModelMapper.map(
            currentlyPlaying = audioHandler.currentlyPlaying,
            trackQueue = audioHandler.trackQueue,
            isLooping = audioHandler.isLooping
        )
    }

    fun getCurrentlyPlayingTrack(
        apiKey: String
    ): CurrentlyPlayingModel {
        val user = authenticateUser(apiKey)
        val guild = discord.guildFromId(user.guildId)

        val track = (guild?.let { audioHandlers.get(guild) } ?: throw notConnectedException).currentlyPlaying
        return trackModelMapper.mapCurrentlyPlaying(track)
    }

    fun setLooping(apiKey: String, request: LoopRequest) {
        val user = authenticateUser(apiKey)
        val guild = discord.guildFromId(user.guildId)
        (guild?.let { audioHandlers.get(guild) } ?: throw notConnectedException).isLooping = request.loop
    }

    private fun searchResultCallback(
        query: String,
        continuation: Continuation<TrackQueuedResponse>
    ) = object : SearchResultCallback {

        override fun onTracksQueued(numberOfTracks: Int) {
            continuation.resume(TrackQueuedResponse(numberOfTracks))
        }

        override fun onError(throwable: Throwable) {
            log.error("Error searching for $query", throwable)
            continuation.resumeWithException(throwable)
        }
    }

}
