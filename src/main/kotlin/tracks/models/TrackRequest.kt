package tracks.models

import kotlinx.serialization.Serializable

@Serializable
data class TrackRequest(
    val query: String,
    val startImmediately: Boolean
)
