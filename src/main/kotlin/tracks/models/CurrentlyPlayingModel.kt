package tracks.models

import kotlinx.serialization.Serializable

@Serializable
data class CurrentlyPlayingModel(
    val currentlyPlaying: TrackModel?
) {

    companion object {
        fun none() = CurrentlyPlayingModel(null)
    }
}
