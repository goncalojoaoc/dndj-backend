package tracks.models

import kotlinx.serialization.Serializable

@Serializable
data class LoopRequest(
    val loop: Boolean
)
