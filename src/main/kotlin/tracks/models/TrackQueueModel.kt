package tracks.models

import kotlinx.serialization.Serializable

@Serializable
data class TrackQueueModel(
    val currentlyPlaying: TrackModel?,
    val queuedTracks: List<TrackModel>,
    val isLooping: Boolean
)
