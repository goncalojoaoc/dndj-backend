package tracks.models.mapper

import com.sedmelluq.discord.lavaplayer.track.AudioTrack
import tracks.models.TrackQueueModel

class QueueModelMapper(
    private val trackModelMapper: TrackModelMapper
) {

    fun map(currentlyPlaying: AudioTrack?, trackQueue: List<AudioTrack>, isLooping: Boolean): TrackQueueModel =
        TrackQueueModel(
            currentlyPlaying = currentlyPlaying?.let { trackModelMapper.map(currentlyPlaying) },
            queuedTracks = trackQueue.map { trackModelMapper.map(it) },
            isLooping = isLooping
        )
}
