package tracks.models.mapper

import com.sedmelluq.discord.lavaplayer.track.AudioTrack
import tracks.models.CurrentlyPlayingModel
import tracks.models.TrackModel

class TrackModelMapper {

    fun map(audioTrack: AudioTrack) = with(audioTrack.info) {
        TrackModel(
            title = title.orEmpty(),
            author = author.orEmpty(),
            length = length,
            identifier = identifier.orEmpty(),
            isStream = isStream,
            uri = uri.orEmpty()
        )
    }

    fun map(tracks: List<AudioTrack>): List<TrackModel> = tracks.map { map(it) }

    fun mapCurrentlyPlaying(audioTrack: AudioTrack?) = CurrentlyPlayingModel(
        audioTrack?.let { map(it) }
    )
}
