package tracks.models

import kotlinx.serialization.Serializable

@Serializable
data class TrackModel(
    val title: String,
    val author: String,
    val length: Long,
    val identifier: String,
    val isStream: Boolean,
    val uri: String
)
