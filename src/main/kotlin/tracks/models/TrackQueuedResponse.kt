package tracks.models

import kotlinx.serialization.Serializable

@Serializable
data class TrackQueuedResponse(
    val songsQueued: Int
)
