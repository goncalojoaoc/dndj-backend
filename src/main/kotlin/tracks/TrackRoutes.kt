package tracks

import extensions.doSafely
import io.ktor.application.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import tracks.models.CurrentlyPlayingModel
import tracks.models.LoopRequest
import tracks.models.TrackRequest
import org.koin.ktor.ext.inject

fun Route.registerTrackRoutes() {
    route("/tracks") {
        queueRoutes()
    }
}

fun Route.queueRoutes() {

    val service: TracksService by inject()

    route("/currentlyPlaying") {
        get {
            doSafely { apiKey ->
                call.respond(service.getCurrentlyPlayingTrack(apiKey))
            }
        }
    }

    route("/queue") {
        get {
            doSafely { apiKey ->
                call.respond(service.getQueue(apiKey))
            }
        }

        post {
            doSafely { apiKey ->
                val request = call.receive<TrackRequest>()
                val response = service.queue(
                    apiKey,
                    request
                )
                call.respond(response)
            }
        }
    }

    route("/loop") {
        post {
            doSafely { apiKey ->
                val request = call.receive<LoopRequest>()
                service.setLooping(apiKey, request)
                call.respond(request)
            }
        }
    }

    route("/clear") {
        post {
            doSafely { apiKey ->
                service.clearQueue(apiKey)

                call.respond(CurrentlyPlayingModel.none())
            }
        }
    }
}
