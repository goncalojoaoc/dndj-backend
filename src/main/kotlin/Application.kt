import channels.registerChannelsRoutes
import discord.Discord
import io.ktor.application.*
import io.ktor.features.*
import io.ktor.gson.*
import io.ktor.html.*
import io.ktor.http.*
import io.ktor.routing.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.html.*
import tracks.registerTrackRoutes
import org.koin.ktor.ext.Koin
import org.koin.ktor.ext.inject
import org.koin.logger.slf4jLogger
import org.slf4j.event.Level

fun HTML.index() {
    head {
        title("Hello from Ktor!")
    }
    body {
        div {
            +"Hello from Ktor"
        }
    }
}

fun main(args: Array<String>): Unit = io.ktor.server.netty.EngineMain.main(args)

fun Application.main() {
    install(Koin) {
        modules(appModule)
        slf4jLogger()
    }
    install(CallLogging) { level = Level.INFO }
    install(ContentNegotiation) {
        gson {
            setPrettyPrinting()
        }
    }

    GlobalScope.launch {
        val discord: Discord by inject()
        discord.init()
    }
}

fun Application.module() {
    routing {
        route("/api/v1") {
            registerChannelsRoutes()
            registerTrackRoutes()
        }
    }

    routing {
        get("/") {
            call.respondHtml(HttpStatusCode.OK, HTML::index)
        }
    }
}