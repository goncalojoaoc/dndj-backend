package discord.audio

import net.dv8tion.jda.api.entities.Guild

interface IAudioHandlers {

    fun get(guild: Guild): GuildAudioHandler?

}
