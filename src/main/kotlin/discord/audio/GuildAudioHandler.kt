package discord.audio

import com.sedmelluq.discord.lavaplayer.player.AudioLoadResultHandler
import com.sedmelluq.discord.lavaplayer.tools.FriendlyException
import com.sedmelluq.discord.lavaplayer.track.AudioPlaylist
import com.sedmelluq.discord.lavaplayer.track.AudioTrack
import io.ktor.features.*
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.entities.channel.concrete.VoiceChannel

/**
 * One instance should be created for each guild
 */
class GuildAudioHandler(
    private val audioPlayerManager: GuildAudioPlayerManager,
    private val guild: Guild
) {

    private val trackScheduler: TrackScheduler

    val trackQueue: List<AudioTrack>
        get() = trackScheduler.trackQueue

    val currentlyPlaying: AudioTrack?
        get() = trackScheduler.currentlyPlaying

    val isConnected: Boolean
        get() = guild.audioManager.isConnected

    var isLooping: Boolean
        get() = trackScheduler.loop
        set(value) {
            trackScheduler.loop = value
        }

    val connectedChannel: VoiceChannel?
        get() = guild.audioManager.connectedChannel?.asVoiceChannel()

    init {
        val player = audioPlayerManager.createPlayer()
        trackScheduler = TrackScheduler(player)
        player.addListener(trackScheduler)
        guild.audioManager.sendingHandler = LavaPlayerAudioHandler(player)
    }

    fun connect(voiceChannel: VoiceChannel) {
        with(guild.audioManager) {
            if (!isConnected) {
                openAudioConnection(voiceChannel)
            }
        }
    }

    fun disconnect() {
        with(guild.audioManager) {
            trackScheduler.clear()
            closeAudioConnection()
        }
    }

    fun queue(query: String, clearPrevious: Boolean, callback: SearchResultCallback) {
        audioPlayerManager.search(
            query,
            TrackLoadResultHandler(
                searchResultCallback = callback,
                clearQueueBeforePlaying = clearPrevious
            )
        )
    }

    fun clear() = trackScheduler.clear()

    private inner class TrackLoadResultHandler(
        private val searchResultCallback: SearchResultCallback,
        private val clearQueueBeforePlaying: Boolean = false
    ) : AudioLoadResultHandler {

        override fun trackLoaded(track: AudioTrack?) {
            if (clearQueueBeforePlaying) {
                trackScheduler.clear()
            }
            track?.let { trackScheduler.queue(track) }
            searchResultCallback.onTracksQueued(if (track != null) 1 else 0)
        }

        override fun playlistLoaded(playlist: AudioPlaylist?) {
            if (clearQueueBeforePlaying) {
                trackScheduler.clear()
            }
            playlist?.tracks?.forEach { trackScheduler.queue(it) }
            searchResultCallback.onTracksQueued(playlist?.tracks?.size ?: 0)
        }

        override fun noMatches() {
            searchResultCallback.onError(NotFoundException("No matches"))
        }

        override fun loadFailed(exception: FriendlyException?) {
            searchResultCallback.onError(exception ?: Exception("No error retrieved"))
        }
    }

    interface SearchResultCallback {

        fun onTracksQueued(numberOfTracks: Int)

        fun onError(throwable: Throwable)
    }
}