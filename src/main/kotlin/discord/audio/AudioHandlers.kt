package discord.audio

import net.dv8tion.jda.api.entities.Guild
import java.util.concurrent.ConcurrentHashMap

class AudioHandlers(
    private val audioPlayerManager: GuildAudioPlayerManager
): IAudioHandlers {

    private val audioHandlers: MutableMap<String, GuildAudioHandler> = ConcurrentHashMap()

    override fun get(guild: Guild): GuildAudioHandler? =
        if (audioHandlers.containsKey(guild.id)) {
            audioHandlers[guild.id]
        } else {
            val handler = GuildAudioHandler(audioPlayerManager, guild)
            audioHandlers[guild.id] = handler
            handler
        }
}
