package discord.audio

import com.sedmelluq.discord.lavaplayer.player.AudioPlayer
import com.sedmelluq.discord.lavaplayer.player.event.AudioEventAdapter
import com.sedmelluq.discord.lavaplayer.tools.FriendlyException
import com.sedmelluq.discord.lavaplayer.track.AudioTrack
import com.sedmelluq.discord.lavaplayer.track.AudioTrackEndReason
import org.slf4j.LoggerFactory
import java.util.*
import java.util.concurrent.ConcurrentLinkedQueue

class TrackScheduler(
    private val player: AudioPlayer
) : AudioEventAdapter() {

    private val log = LoggerFactory.getLogger(this::class.java)

    private val queue: Queue<AudioTrack> = ConcurrentLinkedQueue()

    /**
     * Returns a list copy of the current queue
     */
    val trackQueue: List<AudioTrack>
        get() = queue.toList()

    val currentlyPlaying: AudioTrack?
        get() = player.playingTrack

    var loop: Boolean = false

    fun queue(audioTrack: AudioTrack) {
        queue.add(audioTrack)
        if (currentlyPlaying == null) {
            playNext()
        }
    }

    fun clear() {
        player.stopTrack()
        queue.clear()
    }

    override fun onPlayerPause(player: AudioPlayer?) {
        // no-op
    }

    override fun onPlayerResume(player: AudioPlayer?) {
        // no-op
    }

    override fun onTrackStart(player: AudioPlayer?, track: AudioTrack?) {
    }

    override fun onTrackEnd(player: AudioPlayer?, track: AudioTrack?, endReason: AudioTrackEndReason?) {
        if (endReason?.mayStartNext == true) {
            log.debug("Starting next track")
            if (loop) queue.add(track?.makeClone())
            playNext()
        }
        // endReason == FINISHED: A track finished or died by an exception (mayStartNext = true).
        // endReason == LOAD_FAILED: Loading of a track failed (mayStartNext = true).
        // endReason == STOPPED: The player was stopped.
        // endReason == REPLACED: Another track started playing while this had not finished
        // endReason == CLEANUP: Player hasn't been queried for a while, if you want you can put a
        //                       clone of this back to your queue
    }

    override fun onTrackException(player: AudioPlayer?, track: AudioTrack?, exception: FriendlyException?) {
        log.error("onTrackException", exception)
    }

    override fun onTrackStuck(player: AudioPlayer?, track: AudioTrack?, thresholdMs: Long) {
        log.warn("onTrackStuck for ${thresholdMs}ms | track uri: ${track?.info?.uri}")
    }

    private fun playNext() {
        player.playTrack(queue.poll())
    }
}
