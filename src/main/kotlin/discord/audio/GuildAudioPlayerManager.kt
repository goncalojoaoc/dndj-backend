package discord.audio

import com.sedmelluq.discord.lavaplayer.player.AudioLoadResultHandler
import com.sedmelluq.discord.lavaplayer.player.AudioPlayer
import com.sedmelluq.discord.lavaplayer.player.AudioPlayerManager
import com.sedmelluq.discord.lavaplayer.source.AudioSourceManagers
import java.util.concurrent.Future

class GuildAudioPlayerManager(
    private val audioPlayerManager: AudioPlayerManager
) {

    init {
        AudioSourceManagers.registerRemoteSources(audioPlayerManager)
    }

    fun createPlayer(): AudioPlayer = audioPlayerManager.createPlayer()

    fun search(query: String, handler: AudioLoadResultHandler): Future<Void> =
        audioPlayerManager.loadItem(query, handler)

}
