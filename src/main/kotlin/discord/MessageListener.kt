package discord

import authentication.IUserAuthRepository
import authentication.qr.IQRCodeGenerator
import discord.audio.GuildAudioHandler
import discord.audio.IAudioHandlers
import discord.message.*
import kotlinx.coroutines.CancellableContinuation
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.suspendCancellableCoroutine
import net.dv8tion.jda.api.entities.User
import net.dv8tion.jda.api.events.message.MessageReceivedEvent
import net.dv8tion.jda.api.hooks.ListenerAdapter
import net.dv8tion.jda.api.utils.FileUpload
import java.io.File
import kotlin.coroutines.resume

class MessageListener(
    private val repository: IUserAuthRepository,
    private val qrCodeGenerator: IQRCodeGenerator,
    private val messageMapper: IMessageMapper,
    private val audioHandlers: IAudioHandlers
) : ListenerAdapter() {

    companion object {
        private const val MSG_SUCCESSFULLY_AUTHORIZED = "DnDJ has been authorized - please check your DMs!"
        private const val ERROR_NO_DMS_ALLOWED = "Please use the authorize command from a server channel"
        private const val ERROR_GENERIC = "This is a generic error message. #earlyAccess"
        private const val ERROR_USER_NOT_IN_VOICE = "Please join a voice channel first"
    }

    override fun onMessageReceived(event: MessageReceivedEvent) {
        if (event.author.isBot) return
        when (val message = messageMapper.mapContent(event.message.contentRaw)) {
            is IgnoredMessage -> {
            }
            is ClearMessage -> GlobalScope.launch { clearCommand(event) }
            is AuthorizeMessage -> GlobalScope.launch { authorizeCommand(event) }
            is PlayMessage -> GlobalScope.launch { playCommand(event, message.url) }
            is DisconnectMessage -> disconnectFromServer(event)
        }
    }

    private fun disconnectFromServer(event: MessageReceivedEvent) {
        audioHandlers.get(event.guild)?.disconnect()
    }

    private suspend fun playCommand(event: MessageReceivedEvent, url: String) {
        val msg = suspendCancellableCoroutine<String> { continuation ->
            val audioHandler = audioHandlers.get(event.guild)
            if (audioHandler == null) continuation.resume(ERROR_GENERIC)
            audioHandler?.connectIfNeeded(continuation, event)
            audioHandler?.queue(url, false, object : GuildAudioHandler.SearchResultCallback {
                override fun onTracksQueued(numberOfTracks: Int) {
                    continuation.resume(
                        "Successfully queued $numberOfTracks song${if (numberOfTracks > 1) "s" else ""}"
                    )
                }

                override fun onError(throwable: Throwable) {
                    continuation.resume("Unable to queue requested url: ${throwable.message}")
                }
            })
        }
        event.channel.sendMessage(msg)
    }

    private suspend fun clearCommand(event: MessageReceivedEvent) {
        val msg = suspendCancellableCoroutine<String> { continuation ->
            val audioHandler = audioHandlers.get(event.guild)
            if (audioHandler == null) continuation.resume(ERROR_GENERIC)
            audioHandler?.clear()
            continuation.resume("Queue has been cleared.")
        }
        event.channel.sendMessage(msg)
    }

    private fun GuildAudioHandler.connectIfNeeded(
        continuation: CancellableContinuation<String>,
        event: MessageReceivedEvent
    ) {
        if (!isConnected) {
            val voiceChannel =
                event.guild.voiceChannelCache.toList().firstOrNull { it.members.any { it.user == event.author } }
            if (voiceChannel == null) {
                continuation.resume(ERROR_USER_NOT_IN_VOICE)
            } else {
                connect(voiceChannel)
            }
        }
    }

    private suspend fun authorizeCommand(event: MessageReceivedEvent) {
        if (!event.isFromGuild) {
            event.author.dmReplyWith(ERROR_NO_DMS_ALLOWED)
        } else {
            val key = repository.createUserKey(event.author.id, event.guild.id)
            val qrCode = qrCodeGenerator.generate(key)
            event.author.dmReplyWith(formatReply(event.guild.name, key), qrCode)
            event.channel.sendMessage(MSG_SUCCESSFULLY_AUTHORIZED).queue()
        }
    }

    private fun User.dmReplyWith(message: String, file: File? = null) {
        openPrivateChannel().queue { channel ->
            channel.sendMessage(message).queue()
            file?.let { channel.sendFiles(FileUpload.fromData(file)).queue() }
        }
    }


    private fun formatReply(
        guildName: String,
        key: String
    ) = """
        You are now authorized to use DnDJ in $guildName!
        To connect, use the following API Key: $key
        Using the mobile app, you can scan also the following QR code:
        """.trimIndent()
}