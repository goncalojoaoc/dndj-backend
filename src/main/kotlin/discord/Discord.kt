package discord

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import net.dv8tion.jda.api.JDA
import net.dv8tion.jda.api.JDABuilder
import net.dv8tion.jda.api.entities.Activity
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.entities.channel.concrete.VoiceChannel
import net.dv8tion.jda.api.requests.GatewayIntent
import org.slf4j.LoggerFactory

class Discord(
    private val messageListener: MessageListener
) {

    companion object {
        private const val BOT_TOKEN_ENV = "BOT_TOKEN"
    }

    private val token: String = System.getenv(BOT_TOKEN_ENV) ?: ""

    private val log = LoggerFactory.getLogger("DiscordBotService")

    private lateinit var jda: JDA

    suspend fun init() {
        if (token.isEmpty()) {
            log.error("No Discord bot token provided, please check the $BOT_TOKEN_ENV environment variable")
            throw IllegalArgumentException("No Discord bot token provided")
        }

        jda = withContext(Dispatchers.IO) {
            val jdaBuilder = JDABuilder.createDefault(token)
            jdaBuilder.setActivity(Activity.listening("!authorize"))
            jdaBuilder.addEventListeners(messageListener)
            jdaBuilder.enableIntents(GatewayIntent.MESSAGE_CONTENT)
            @Suppress("BlockingMethodInNonBlockingContext")
            jdaBuilder.build().awaitReady()
        }
    }

    fun guildFromId(guildId: String): Guild? = jda.getGuildById(guildId)

    fun channelFromId(channelId: String): VoiceChannel? = jda.getVoiceChannelById(channelId)

}