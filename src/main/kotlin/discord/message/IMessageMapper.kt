package discord.message

interface IMessageMapper {

    fun mapContent(msg: String): Message

}
