package discord.message

class MessageMapper: IMessageMapper {

    companion object {
        private const val AUTHORIZE_COMMAND = "!authorize"
        private const val PLAY_COMMAND = "!play"
        private const val DISCONNECT_COMMAND = "!leave"
        private const val CLEAR_COMMAND = "!clear"

        @Suppress("RegExpRedundantEscape")
        private val PLAY_COMMAND_PATTERN =
            Regex("""$PLAY_COMMAND (https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&\/=]*))""")

        // TODO implement this
        private val UNAUTHORIZE_COMMAND_PATTERN = Regex("!unauthorize .*")
    }

    override fun mapContent(msg: String) = when {
        msg.trim() == AUTHORIZE_COMMAND -> AuthorizeMessage
        msg.trim() == DISCONNECT_COMMAND -> DisconnectMessage
        msg.trim() == CLEAR_COMMAND -> ClearMessage
        msg.contains(PLAY_COMMAND) -> mapPlayMessage(msg)
        else -> IgnoredMessage
    }

    private fun mapPlayMessage(msg: String): Message =
        PLAY_COMMAND_PATTERN.find(msg)?.let { match ->
            match.groups[1]?.let { url ->
                PlayMessage(url.value)
            }
        } ?: IgnoredMessage

}