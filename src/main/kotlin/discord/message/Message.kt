package discord.message

sealed class Message {
}

object AuthorizeMessage : Message()

object IgnoredMessage: Message()

data class PlayMessage(
    val url: String
): Message()

object DisconnectMessage: Message()

object ClearMessage : Message()
