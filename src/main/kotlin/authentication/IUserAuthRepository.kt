package authentication

import authentication.models.AuthUser

interface IUserAuthRepository {

    /**
     * Creates a key associated with the user, stores it, and returns it
     */
    fun createUserKey(userId: String, guildId: String): String

    /**
     * Returns the user information associated with the key. Returns null if no user exists with that key
     */
    fun getAuthenticatedUser(key: String): AuthUser?
}