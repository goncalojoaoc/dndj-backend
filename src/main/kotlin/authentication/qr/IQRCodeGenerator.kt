package authentication.qr

import java.io.File

interface IQRCodeGenerator {

    suspend fun generate(message: String): File?

}
