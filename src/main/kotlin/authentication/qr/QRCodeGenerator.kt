package authentication.qr

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import net.glxn.qrgen.core.image.ImageType
import net.glxn.qrgen.javase.QRCode
import org.slf4j.LoggerFactory
import java.io.File

class QRCodeGenerator : IQRCodeGenerator {

    private val log = LoggerFactory.getLogger(this::class.java)

    override suspend fun generate(message: String): File? = withContext(Dispatchers.Default) {
        try {
            QRCode.from(message).to(ImageType.JPG).withSize(500, 500).file()
        } catch (ex: Exception) {
            log.error("Failed to generate QR code", ex)
            null
        }
    }
}
