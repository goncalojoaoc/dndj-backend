package authentication

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import authentication.models.AuthUser
import org.slf4j.LoggerFactory
import java.io.File
import java.util.*
import java.util.concurrent.ConcurrentHashMap

// TODO: Make the sync to file logic less aggressive - maybe a long running coroutine
class JsonUserAuthRepository(
    private val cacheToMemory: Boolean = true
) : IUserAuthRepository {

    companion object {
        private const val FILE_DIR_PATH = "./db/"
        private const val FILE_PATH = "${FILE_DIR_PATH}users.json"
    }

    private val log = LoggerFactory.getLogger(this::class.simpleName)

    private val gson: Gson = GsonBuilder().setPrettyPrinting().create()
    private val gsonType = object : TypeToken<MutableMap<String, AuthUser>>() {}.type
    private val file: File = File(FILE_PATH)

    private var cache: MutableMap<String, AuthUser> = ConcurrentHashMap()

    init {
        initFileIfNeeded()
    }

    override fun createUserKey(userId: String, guildId: String): String =
        if (cacheToMemory) {
            createInMemoryThenSync(userId, guildId)
        } else {
            createInFile(userId, guildId)
        }


    override fun getAuthenticatedUser(key: String): AuthUser? =
        if (cacheToMemory) {
            getFromCache(key)
        } else {
            getFromFile(key)
        }

    private fun createInFile(userId: String, guildId: String): String {
        val key = UUID.randomUUID().toString()
        val cache = readFile()
        cache[key] = AuthUser(userId, guildId, key)
        writeFile(cache)
        return key
    }

    private fun createInMemoryThenSync(userId: String, guildId: String): String {
        val key = UUID.randomUUID().toString()
        cache[key] = AuthUser(userId, guildId, key)
        writeFile(cache)
        return key
    }

    private fun getFromFile(key: String): AuthUser? = readFile()[key]

    private fun getFromCache(key: String): AuthUser? {
        var user = cache[key]
        if (user == null) {
            refreshCache()
            user = cache[key]
        }
        return user
    }

    private fun refreshCache() {
        cache = readFile()
    }

    private fun readFile(): MutableMap<String, AuthUser> = gson.fromJson(file.readText(), gsonType)

    private fun writeFile(map: MutableMap<String, AuthUser>) {
        file.writeText(gson.toJson(map))
    }

    private fun initFileIfNeeded() {
        File(FILE_DIR_PATH).mkdirs()
        val wasCreated = file.createNewFile()
        if (wasCreated) {
            log.debug("Created file: $FILE_PATH")
            file.writeText(gson.toJson(cache))
        } else if (cacheToMemory) {
            refreshCache()
        }
    }
}