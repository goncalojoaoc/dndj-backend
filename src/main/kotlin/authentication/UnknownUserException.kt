package authentication

class UnknownUserException(message: String?) : Exception(message)
