package authentication.models

data class AuthUser(
    val userId: String,
    val guildId: String,
    val authKey: String
)
