import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.6.0"
    application
    id("org.jetbrains.kotlin.plugin.serialization") version "1.6.0"
}

group = "me.goncalo"
version = "1.0-SNAPSHOT"

repositories {
    jcenter()
    mavenCentral()
    maven(url = "https://m2.dv8tion.net/releases")
    maven(url = "https://jitpack.io")
}

dependencies {
    // KTOR stuff
    implementation("io.ktor:ktor-server-netty:1.5.4")
    implementation("io.ktor:ktor-html-builder:1.5.4")
    implementation("io.ktor:ktor-serialization:1.5.4")
    implementation("io.ktor:ktor-gson:1.5.4")
    implementation("org.jetbrains.kotlinx:kotlinx-html-jvm:0.7.2")
    implementation("ch.qos.logback:logback-classic:1.2.3")

    // Dependency injection
    implementation("io.insert-koin:koin-core:3.0.1")
    implementation("io.insert-koin:koin-ktor:3.0.1")
    implementation("io.insert-koin:koin-core-ext:3.0.1")
    implementation("io.insert-koin:koin-logger-slf4j:3.0.1")

    // Discord API stuff
    implementation("net.dv8tion:JDA:5.0.0-beta.6")
    implementation("com.sedmelluq:lavaplayer:1.3.77")

    // QR codes for coolness
    implementation("com.github.kenglxn.qrgen:javase:2.6.0")

    // Testing stuff
    testImplementation(kotlin("test-junit"))
    testImplementation("io.insert-koin:koin-test:3.0.1")
    testImplementation("org.mockito:mockito-core:3.10.0")
    testImplementation("org.mockito.kotlin:mockito-kotlin:3.2.0")
}

tasks.test {
    useJUnit()
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}

application {
    mainClass.set("ApplicationKt")
}

extensions.findByName("buildScan")?.withGroovyBuilder {
    setProperty("termsOfServiceUrl", "https://gradle.com/terms-of-service")
    setProperty("termsOfServiceAgree", "yes")
}