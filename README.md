[![pipeline status](https://gitlab.com/goncalojoaoc/dndj-backend/badges/master/pipeline.svg)](https://gitlab.com/goncalojoaoc/dndj-backend/-/commits/master)

# DnDJ Discord Bot

This is a Discord music bot specially designed for running D&D sessions with backing soundtracks. Its main purpose it to provide an API that can be called by a mobile app to quickly change between tracks, instead of having to mess around with message commands, which take time to write and can end up polluting the chat.

## Features
- Per user, per server authorization via a generated api key
- Join/Leave any server voice channel (as long as the bot is given a role with sufficient permissions)
- Queue tracks individually or via youtube playlists
- Queue track to be played immediately, stopping any currently playing track and clearing the queue
- View and clear the music track queue
- Enable/disable looping of the queue

## Companion mobile app
Coming soon(ish) to Android.

## Install
This service is setup to run as a docker container, so make sure you've got docker installed and running.

You will need a bot token, which you can create using Discord's developer console. Make sure it has enough permissions to see and join/write to channels.

### docker-compose (recommended)
```
version: "2.1"
services:
  dndj:
    image: registry.gitlab.com/goncalojoaoc/dndj-backend:latest
    container_name: dndj
    environment:
      - BOT_TOKEN=<your discord bot token>
    volumes:
      - /path/to/dndj/database/folder/:/app/bin/db
    ports:
      - 8080:8080
    restart: unless-stopped
```

### Docker CLI
```
docker run -d \
  --name=dndj \
  -e BOT_TOKEN=<your discord bot token> \
  -p 8080:8080 \
  -v /path/to/dndj/database/folder/:/app/bin/db \
  --restart unless-stopped \
  registry.gitlab.com/goncalojoaoc/dndj-backend
```

## Parameters
| Parameter | Function |
| ------ | ------ |
| `-e BOT_TOKEN=...` | Discord bot token, which needs to be provided as an environment variable |
| `-p 8080` | Allows http access to the internal webserver |
| `-v /app/bin/db` | Path to where the user database file will be saved |

## API documentation
Coming soon(ish)
